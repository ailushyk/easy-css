module.exports = ({ webpack }) => {
  const prodMod = webpack.mode === 'production'

  return ({
      plugins: {
        'postcss-preset-env': {
          stage: 3,
          features: {
            'nesting-rules': true
          },
          autoprefixer: prodMod
        },
        'postcss-sorting': {},
        'cssnano': prodMod ? {
          preset: 'default'
        } : false
      }
    }
  )
}
