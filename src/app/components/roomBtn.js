import { button } from '../../ui/button'

export const roomBtn = ({ label, classes = [] }) => {
  const element = button()
  element.textContent = label
  classes.forEach(c => element.classList.add(c))
  return element
}
