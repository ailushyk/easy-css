import { stand } from '../../ui/stand'

export const standComponent = ({ children = [] }) => {
  const exStand = stand()
  exStand.classList.add('room__stand')
  children.forEach(c => exStand.appendChild(c))
  return exStand
}
