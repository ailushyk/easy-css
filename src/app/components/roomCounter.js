import { counter } from '../../ui/counter'

export const roomCounter = () => {
  const element = counter()
  element.classList.add('room__counter')
  return element
}
