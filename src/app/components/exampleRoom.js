import { room } from '../../ui/room'
import { roomBtn } from './roomBtn'
import { roomCounter } from './roomCounter'
import { standComponent } from './roomStand'

export const exampleRoom = () => {
  const component = room()

  component.appendChild(standComponent({
    children: [
      roomCounter(),
      roomBtn({
        label: 'example',
      })
    ]
  }))

  component.appendChild(standComponent({
    children: [
      roomCounter(),
      roomBtn({
        label: 'link',
        classes: ['btn--link']
      })
    ]
  }))

  component.appendChild(standComponent({
    children: [
      roomCounter(),
      roomBtn({
        label: 'outline',
        classes: ['btn--outline']
      })
    ]
  }))

  const disabledBtn = roomBtn({
    label: 'disabled',
  })
  disabledBtn.disabled = true
  component.appendChild(standComponent({
    children: [
      roomCounter(),
      disabledBtn
    ]
  }))

  component.appendChild(standComponent({
    children: [
      roomCounter(),
      roomBtn({
        label: 'raised',
        classes: ['btn--raised']
      })
    ]
  }))

  component.appendChild(standComponent({
    children: [
      roomCounter(),
      roomBtn({
        label: 'danger',
        classes: ['btn--danger']
      })
    ]
  }))

  return component
}
