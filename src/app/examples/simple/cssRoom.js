import { exampleRoom } from '../../components/exampleRoom'
import { header } from '../../../ui/header'
import './css/main.css'

export const cssRoom = () => {
  const component = document.createElement('div')
  component.setAttribute('id', 'css-room')
  component.append(header({ h: 'h2', title: 'CSS example' }))
  component.append(exampleRoom())
  return component
}
