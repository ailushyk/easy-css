import { header } from '../../../ui/header'
import { exampleRoom } from '../../components/exampleRoom'
import './css/main.css'

export const postcssRoom = () => {
  const component = document.createElement('div')
  component.setAttribute('id', 'postcss-room')
  component.append(header({ h: 'h2', title: 'PostCSS example' }))
  component.append(exampleRoom())
  return component
}
