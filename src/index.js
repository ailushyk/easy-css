import { postcssRoom } from './app/examples/postcss/postcssRoom'
import { cssRoom } from './app/examples/simple/cssRoom'
import { header } from './ui/header'

const app = document.querySelector('#app')

app.append(header({ h: 'h1', title: 'Comparison' }))
app.appendChild(postcssRoom())
// app.appendChild(cssRoom())
