export const header = ({
  h,
  title
}) => {
  const component = document.createElement(h)
  component.innerText = title
  return component
}
